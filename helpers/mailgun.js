/** 
 *  Send an email using mailgun
 *  @example: 
 *       {from: 'SAVOIETC <no-reply@misitioba.com>',to: 'arancibiajav@gmail.com',subject: 'SAVOIETC Contact form triggered',text: `Hello Foo`}
 */
module.exports = function sendMailgun(data) {
    return new Promise(async (resolve, reject) => {
        let mailgun = await getMailgun()
        mailgun.messages().send(data, function (error, body) {
            if (error) {
                reject(error)
            } else {
                resolve(body)
            }
        })
    })
}

async function getMailgun() {
    var mailgun = require('mailgun-js')({ apiKey: process.env.MAILGUN_API_KEY, domain: process.env.MAILGUN_DOMAIN });
    return mailgun;
}

