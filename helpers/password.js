const bcrypt = require("bcrypt");
const saltRounds = 10;
module.exports= function generate() {
    const shortid = require('shortid');
    let password = shortid.generate()
    const salt = bcrypt.genSaltSync(saltRounds);
    return {
        hash: bcrypt.hashSync(password, salt),
        password
    };
}